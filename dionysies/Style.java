import java.util.Random;

public enum Style {
    COMÉDIE,
    TRAGÉDIE,
    DRAME;

    public String toString() {
	if (this == COMÉDIE)
	    return "Comédie";
	else if (this == TRAGÉDIE)
	    return "Tragédie";
	else {
	    assert this == DRAME;
	    return "Drame";
	}
    }

	public static Style randomStyle(){
		Random rand = new Random();
		int nb = rand.nextInt(Style.values().length);
		return Style.values()[nb];
	}
}