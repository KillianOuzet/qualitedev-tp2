public class Journee {

    private Epreuve epreuveMatin;
    private Epreuve epreuveAprem;
    private Epreuve epreuveSoir;

    public Journee(){
        this.epreuveMatin = new Epreuve(Moment.MATIN);
        this.epreuveAprem = new Epreuve(Moment.APRESMIDI);
        this.epreuveSoir = new Epreuve(Moment.SOIR);
    }

    public String afficheCitations(Auteur candidat1){
        return "La citation de la première épreuve de: " + candidat1.getNom() + " est: " + candidat1.citationStyle(epreuveMatin.getStyle()) + ("\n") + "La citation de la deuxième épreuve de: " + candidat1.getNom() + " est: " + candidat1.citationStyle(epreuveAprem.getStyle()) + ("\n") + "La citation de la dernière épreuve de: " + candidat1.getNom() + " est: " + candidat1.citationStyle(epreuveSoir.getStyle()) + ("\n");
    }

    public int scoreJournee(Auteur auteur){
        return epreuveMatin.scoreAuteur(auteur) + epreuveAprem.scoreAuteur(auteur) + epreuveSoir.scoreAuteur(auteur);
    }
}