public class Auteur {
    private String nom;
    private int qualiteComedie;
    private String citationComedie;
    private int qualiteTragedie;
    private String citationTragedie;
    private int qualiteDrame;
    private String citationDrame;

    public Auteur(String nom, int qualite1, String citation1, int qualite2, String citation2, int qualite3, String citation3){
        this.nom = nom;
        this.qualiteComedie = qualite1;
        this.citationComedie = citation1;
        this.qualiteTragedie = qualite2;
        this.citationTragedie = citation2;
        this.qualiteDrame = qualite3;
        this.citationDrame = citation3;
    }

    public String getNom(){
        return this.nom;
    }

    public int getQualiteTragedie(){
        return  this.qualiteTragedie;
    }

    public String getCitationTragedie(){
        return this.citationTragedie;
    }

    public int getQualiteComedie(){
        return  this.qualiteComedie;
    }

    public String getCitationComedie(){
        return this.citationComedie;
    }

    public int getQualiteDrame(){
        return  this.qualiteDrame;
    }

    public String getCitationDrame(){
        return this.citationDrame;
    }

    public Style pointFort(){
        Style meilleurStyle = null;
        int meilleurQualite = 0;
        if(this.qualiteComedie > this.qualiteTragedie){
            meilleurStyle = Style.COMÉDIE;
            meilleurQualite = this.qualiteComedie;
        }else{
            meilleurStyle = Style.TRAGÉDIE;
            meilleurQualite = this.qualiteTragedie;
        }
        if(this.qualiteDrame > meilleurQualite){
            meilleurStyle = Style.DRAME;
            meilleurQualite = this.qualiteComedie;
            return meilleurStyle;
        }
        return meilleurStyle;
    }

    public int qualiteStyle(Style s){
        if(s.equals(Style.COMÉDIE)){
            return this.qualiteComedie;
        }
        if(s.equals(Style.TRAGÉDIE)){
            return this.qualiteTragedie;
        }
        if(s.equals(Style.DRAME)){
            return this.qualiteDrame;
        }
        return 0;
    }

    public String citationStyle(Style s){
        if(s.equals(Style.COMÉDIE)){
            return this.citationComedie;
        }
        if(s.equals(Style.TRAGÉDIE)){
            return this.citationTragedie;
        }
        if(s.equals(Style.DRAME)){
            return this.citationDrame;
        }
        return "";
    }

    @Override
    public String toString(){
        return String.format("l'honorable %s", this.nom);
    }
}
