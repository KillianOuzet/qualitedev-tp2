public class Epreuve {
    private Style style;
    private int nbSpectateur;
    private String horaire;

    public Epreuve( Moment moment){
        style = Style.randomStyle();
        horaire = moment.toString();
        nbSpectateur = moment.getRandomSpec();
    }

    public Epreuve(Style styledonnée, Moment moment, int nbSpec){
        style = styledonnée;
        horaire = moment.toString();
        nbSpectateur = nbSpec;
    }

    public Style getStyle(){
        return this.style;
    }

    public int scoreAuteur(Auteur auteur){
        return nbSpectateur * auteur.qualiteStyle(this.style);
    }
}
