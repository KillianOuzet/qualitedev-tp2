import java.util.Random;

public enum Moment{
    MATIN("Matin",500,1000),
    APRESMIDI("Apres-midi",1000,2000),
    SOIR("Soir",2000,4000);

    private String momentName; 
    private int minSpec, maxSpec;

    Moment(String name, int minSpec, int maxSpec) {
        this.momentName = name;
        this.minSpec = minSpec;
        this.maxSpec = maxSpec;
    }

    public int getRandomSpec(){
        Random rand = new Random();
		return rand.nextInt(maxSpec - minSpec) + minSpec;
    }

    public String toString() {
        if (this == MATIN)
            return "Matin";
        else if (this == APRESMIDI)
            return "Après-midi";
        else {
            assert this == SOIR;
            return "Soir";
        }
        }
}
